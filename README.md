**This is the repo for our group's css xen garden mini project**

Below, you'll find an explanation of what everyone did.

*Yay*

---

## Braanah

- Created Bitbucket Repo
- Invited groupmates to said repo
- Edited readme template
- hovered
- added zen background photo

---

## Danny

- put the team on his back
- fiddled with display properties

---

## Kim
- change the fonts using Google Font...
- Font I used: Pacifico, Butterfly Kids
- Link color and link hover:color change 
- gradient background

---

## Patrick

- responsiveness things
